# Lab 4 - Puppet Modules

## Lab tutorial

1.  Create a new stack with an Ubuntu instance that has Puppet installed. You can use the [following tutorial.](https://gitlab.stud.idi.ntnu.no/jhklemet/iikg3005-labs/-/blob/master/puppet_vm.md)

2.  Install the `puppetlabs-ntp` and `saz-timezone` modules and write a
    manifest `time.pp` which declares ntp and configures timezone to be
    `Europe/Oslo`. Apply the manifest with `puppet apply` (apply it
    repeatedly until it has converged, meaning it only generates the
    minimum output every time you apply it).
    
        puppet module install puppetlabs-ntp
        puppet module install saz-timezone
        
        # time.pp:
        
        node default {
          include ntp
          class { 'timezone':
            timezone => 'Europe/Oslo',
          }
        }

3.  Install the modules `puppetlabs-apache` and `hunner-wordpress` and
    write a manifest which installs wordpress (according to the README
    at <https://github.com/hunner/puppet-wordpress>)
    
        puppet module install puppetlabs-apache
        puppet module install hunner-wordpress
        puppet module list --tree
        
        # wordpress.pp:
        
        node default {
        
          $mysql_password        = 'put_in_hiera'
          $wordpress_password    = 'put_also_in_hiera'
        
          # PHP-enabled web server
          class { 'apache':
            default_vhost => false,
            mpm_module    => 'prefork',
          }
          class { 'apache::mod::php':
              php_version => '7.2',
          }
        
          # Virtual Host for Wordpress
          apache::vhost { $::fqdn:
            port           => '80',
            docroot        => '/opt/wordpress',
            manage_docroot => false,
          }
        
          # MySQL server
          class { 'mysql::server':
            root_password => $mysql_password,
          }
          class { 'mysql::bindings':  # if added late, need to restart apache service
            php_enable => true,
            php_package_name => 'php-mysql',
          }
        
          # Wordpress
          user { 'wordpress':
            ensure => 'present'
          }
          class { 'wordpress':
            version     => '4.9.7',   # if changed, delete index.php, rerun puppet
                                      # due to creates => "${install_dir}/index.php"
                                      # in manifests/instance/app.pp
            wp_owner    => 'wordpress',
            wp_group    => 'wordpress',
            db_user     => 'wordpress',
            db_password => $wordpress_password,
            require     => [ Class['apache'], Class['mysql::server'], User['wordpress'] ],
          }
        }

## Review questions and problems

1.  Give a brief overview of how you can test Puppet modules (from the
    simplest to the most advanced testing).

2.  Install the module `puppetlabs-motd`. See the [README file](https://forge.puppet.com/puppetlabs/motd) for examples of how
    to use it. Write a Puppet manifest where you where you declare the class `motd` and
    let the parameter `content` be `'motd/motd.epp'`  
    
    Do a `puppet apply`, log out and back in to see the result. Btw, where is the file
    `motd/motd.epp` in the file system?

3.  Write a Puppet module `gossinbackup` which installs and configures
    the code from <https://github.com/githubgossin/gossin-backup>. The
    module should allow the following parameters to be configured:
    
    |                         |                                                            |
    | :---------------------- | :--------------------------------------------------------- |
    | `source_directory`      | (REQUIRED)                                                 |
    | `destination_directory` | (REQUIRED)                                                 |
    | `exclude`               | (default `/opt/gossin-backup/rsyncexcludes`)               |
    | `days`                  | (how many daily backups to keep, default `7`)              |
    | `months`                | (how many monthly backups to keep, default `12`)           |
    | `years`                 | (how many yearly backups to keep default `3`)              |
    | `max_file_size`         | (default `100M`)                                           |
    | `exec_hours`            | (which hours this will execute, default every hour: `*/1`) |
    | `exec_minutes`          | (minutes of hour this will exec, default `[ 15, 45 ]`)     |
    

    Read [Module
    fundamentals](https://puppet.com/docs/puppet/latest/modules_fundamentals.html),
    use the [Beginner’s Guide to
    Modules](https://puppet.com/docs/puppet/latest/bgtm.html) and study
    the puppetlabs-ntp module mentioned there. Your module should have
    the following classes:
    
      - `gossinbackup`
    
      - `gossinbackup::install`
    
      - `gossinbackup::config`
    
    Your module should depend on the `puppetlabs-stdlib` so it can
    define some of the parameters as `Stdlib::Absolutepath` data type.
    
    Your module should depend on the `puppetlabs-vcsrepo` module and use
    it for installing the script by cloning the repo on GitHub.
    
    Your module should depend on the `puppetlabs-inifile` module and use
    it for editing the configuration file (`gossin-backup.conf`).
    
    Simplest possible use of your module should be:
    
        class { 'gossinbackup':
            source_directory      => '/home',
            destination_directory => '/backups',
        }


